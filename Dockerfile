FROM golang:1.14
COPY *.go go.* Makefile .githash /bld/
COPY cmd/ /bld/cmd/

WORKDIR /bld
RUN make

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=0 /bld/filestore .
ENTRYPOINT ["/filestore", "server"]

