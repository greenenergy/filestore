/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/filestore/cmd/app"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "launch the filestore server",
	Long:  `This runs the filestore service.`,
	Run: func(cmd *cobra.Command, args []string) {
		if logs == nil {
			fmt.Println("logs is nil!")
		}
		tst, _ := cmd.Flags().GetBool("test")

		var vr app.VolumeRecorder

		var err error
		if tst {
			vr = app.NewMemVolumeRecorder()
		} else {
			if config == nil {
				log.Fatal("why is config nil?")
			}

			switch config.Get("vmgr.dbengine") {
			case "redis":
				fmt.Println("Creating REDIS backed server")
				addr := config.GetString("vmgr.redis.addr")
				db := config.GetInt("vmgr.redis.db")
				vr, err = app.NewRedisVolumeRecorder(addr, db)
				if err != nil {
					log.Fatal(err.Error())
				}

			case "postgres":
				fmt.Println("Creating POSTGRES backed server")
				varg := config.Get("vmgr.pg").(map[string]interface{})

				vr, err = app.NewPostgresVolumeRecorder(varg)
				if err != nil {
					log.Fatal(err.Error())
				}

			default:
				log.Fatal("unsupported backend - must be redis or postgres")
			}
		}

		paths := config.GetStringMapString("vmgr.paths")
		vm := app.NewVolumeManager(vr, paths)

		mfs := app.NewMyFilestoreServer(logs, config, vm)
		if mfs == nil {
			log.Fatal("why is mfs nil")
		}
		mfs.RunServer()
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)
	serverCmd.Flags().BoolP("verbose", "v", false, "Verbose mode")
	serverCmd.Flags().BoolP("test", "t", false, "test mode (use internal db)")
}
