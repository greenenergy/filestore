/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package cmd

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/spf13/cobra"
	pb "gitlab.com/greenenergy/filestore/cmd/service/filestore"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get <id>",
	Short: "retrieve one or more files",
	Long: `Retrieves one or more files. The output flag value will
be interpreted as a filename if pulling only one file, or a folder if pulling
multiple files (or pulling by any field other than ID)`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Println("missing file ID")
			return
		}

		fileID := args[0]
		token := cmd.Flags().Lookup("token").Value.String()

		client, err := GetClient()
		if err != nil {
			logs.Error.Printf("problem connecting to client: %v", err)
			return
		}

		ctx := context.Background()
		fb := pb.FileBlob{
			ID:          fileID,
			VolumeToken: token,
		}

		stream, err := client.Get(ctx, &fb)
		if err != nil {
			logs.Error.Printf("problem getting stream handle: %v", err)
			return
		}

		reading := true
		first := true
		var out io.WriteCloser
		for reading {
			fb, err := stream.Recv()
			if err != nil {
				if err != io.EOF {
					logs.Error.Printf("problem reading: %v", err)
					return
				}
				reading = false
			} else {
				if first {
					outname := cmd.Flags().Lookup("output").Value.String()
					if outname != "" {
						out, err = os.Create(outname)
						defer out.Close()
						if err != nil {
							log.Fatal(err.Error())
						}
					} else {
						out = os.Stdout
					}
					first = false
				}
				out.Write(fb.Payload)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(getCmd)
	getCmd.Flags().StringP("output", "o", "", "filename for output. If not provided, will be output to stdout")
	getCmd.Flags().StringP("tags", "t", "", "comma separated list of tags to look for")
	getCmd.Flags().StringP("token", "T", "", "Token for the volume you wish to read from")
}
