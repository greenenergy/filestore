/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package cmd

import (
	"strings"

	"github.com/spf13/pflag"
)

// GetTagsAttrs - helper function to process commandline attributes and tags
func GetTagsAttrs(flags *pflag.FlagSet) ([]string, map[string]string) {
	var tags []string
	var attrs map[string]string

	tags = strings.Split(flags.Lookup("tags").Value.String(), ",")
	tmpattrs := flags.Lookup("attributes").Value.String()

	if len(tmpattrs) > 0 {
		parts := tmpattrs[1 : len(tmpattrs)-1]

		// Take a string in the form "a=b,c=d" and turn it into
		// a map[string]string of:
		// {"a":"b", "c":"d"}
		ToMap := func(x string) map[string]string {
			out := make(map[string]string)
			parts := strings.Split(x, ",")
			for _, p := range parts {
				subparts := strings.Split(p, "=")
				if len(subparts) > 1 {
					out[subparts[0]] = subparts[1]
				}
			}
			return out
		}
		attrs = ToMap(parts)
	}
	return tags, attrs
}
