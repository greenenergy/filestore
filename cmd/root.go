/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package cmd

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/filestore/cmd/app"
	pb "gitlab.com/greenenergy/filestore/cmd/service/filestore"
	"google.golang.org/grpc"

	"github.com/spf13/viper"
)

var (
	cfgFile   string
	config    *viper.Viper
	githash   string
	logs      *app.LogSet
	starttime = time.Now()
)

// ConfigStruct - The control structure holding user's config vars.
type ConfigStruct struct {
	Server struct {
		HTTPAddr   string `mapstructure:"httpAddr"`
		LogLevel   string `mapstructure:"logLevel"`
		GRPCAddr   string `mapstructure:"grpcAddr"`
		Debug      bool   `mapstructure:"debug"`
		SigningKey string `mapstructure:"signingKey"`
	}
	Debug struct {
		ListingLimit int `mapstructure:"listingLimit"`
	}
	VMgr struct {
		DB struct {
			Host string `mapstructure:"host"`
			Port int    `mapstructure:"port"`
			Name string `mapstructure:"name"`
			User string `mapstructure:"user"`
			Pass string `mapstructure:"pass"`
		}
		Paths map[string]string `mapstructure:"paths"`
	}
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "filestore",
	Short: "filestore is a file storage service and command program",
	Long: `Filestore is a grpc based file storage microservice. The purpose
 of this package is to allow for the storage and retrieval of files
 across multiple microservices without having to implement awkward NFS sharing
 schemes`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(ls *app.LogSet, hash string) {
	githash = hash
	logs = ls
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringP("server.grpcAddr", "g", "", "address (host:port) to listen on for grpc")
	rootCmd.PersistentFlags().BoolP("verbose", "v", false, "Verbose mode")
}

// GetClient - return a connected client
func GetClient() (pb.FileStoreClient, error) {
	listenAddr := config.GetString("server.grpcAddr")
	conn, err := grpc.Dial(listenAddr, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("problem connecting to grpc: %w", err)
	}
	client := pb.NewFileStoreClient(conn)
	return client, nil
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	config = viper.New()
	config.SetConfigName("filestore")
	config.SetConfigType("toml")
	config.AddConfigPath("/etc")
	config.AddConfigPath("etc")
	config.AddConfigPath(".")
	err := config.ReadInConfig()
	if err != nil {
		logs.Error.Println("Config file used:", config.ConfigFileUsed())
		logs.Error.Println("problem with config file:", err.Error())
		os.Exit(-1)
	}

	err = config.BindPFlags(rootCmd.PersistentFlags())
	if err != nil {
		log.Fatal(err.Error())
	}

	grpcListen := config.GetString("server.grpcAddr")
	if grpcListen == "" {
		log.Fatal("no grpcAddr in server section")
	}

	var cs ConfigStruct
	err = config.Unmarshal(&cs)
	if err != nil {
		log.Fatal(err.Error())
	}

	//tmps, _ := json.MarshalIndent(cs, "", "    ")
	//fmt.Println(string(tmps))
}
