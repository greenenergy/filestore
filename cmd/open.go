/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package cmd

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	pb "gitlab.com/greenenergy/filestore/cmd/service/filestore"
)

// openCmd represents the open command
var openCmd = &cobra.Command{
	Use:   "open <volume>",
	Short: "Open a volume for use",
	Long: `Open a volume for use.
Volumes are collections of file objects, and have attributes
and tags controlling their behaviour, such as what kind of
media to use (SSD, HD, archival) etc.`,
	Run: func(cmd *cobra.Command, args []string) {

		if len(args) < 1 {
			fmt.Println("missing volume name")
			return
		}
		volname := args[0]

		pw := cmd.Flags().Lookup("password").Value.String()

		client, err := GetClient()
		if err != nil {
			logs.Error.Printf("problem connecting to client: %v", err)
			return
		}
		ctx := context.Background()

		vd := pb.VolumeDef{
			Name:     volname,
			Password: pw,
		}

		tags, attrs := GetTagsAttrs(cmd.Flags())

		vd.Tags = tags
		vd.Attrs = attrs

		resvd, err := client.GetVolume(ctx, &vd)
		if err != nil {
			logs.Error.Printf("calling OpenVolume: %v", err)
			return
		}

		tmps, _ := json.MarshalIndent(resvd, "", "    ")
		fmt.Println("resvd:", string(tmps))
	},
}

func init() {
	rootCmd.AddCommand(openCmd)

	openCmd.Flags().StringP("password", "p", "", "password for volume (may be empty)")

	// Flags:
	//   -p password
	//   -T type (fast,normal,slow)

	openCmd.Flags().StringP("tags", "t", "", "Tags to set. Simple comma separated list, no spaces.")

	// The stringslice is nice. You have two ways to use it on the commandline:
	// -a a=b -a c=d
	// or
	// -a a=b,c=d
	// The two forms yield exactly the same thing: [a=b,c=d]
	// compress=true	// use gzip compression
	// compresspriority=speed,size,default,huffman
	openCmd.Flags().StringSliceP("attributes", "a", []string{}, "attributes to set. Use one '-a x=y' per attribute")
}
