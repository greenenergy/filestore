/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package app

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"path"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"gitlab.com/greenenergy/filestore/cmd/service/filestore"
	"golang.org/x/crypto/scrypt"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

const saltsize = 16

// VolumeRecorder - interface for the data backing store.
type VolumeRecorder interface {
	AddFile(vf *VFile, volume *Volume) error
	DeleteFile(id, volumeID string) error
	GetFile(id string, volume *Volume) (*VFile, error)
	AddVolume(v *Volume) error
	GetVolume(name, password, id string) (*Volume, error)
}

// FakeFile - this is for testing, and it takes the place of
// an actual file.
type FakeFile struct {
	bytes.Buffer
}

// Close - just present to complete the interface
// for io.ReadWriteCloser
func (ff *FakeFile) Close() error {
	return nil
}

// VFile - control structure for a volume-backed file
type VFile struct {
	ID       string
	Filename string // Original filename
	Path     string // Path includes filename
	Type     string // MIME file type
	Size     int    // Filesize
	f        io.ReadWriteCloser
	v        *Volume
}

func (vf *VFile) Write(buf []byte) (int, error) {
	nb, err := vf.f.Write(buf)
	vf.Size += nb
	return nb, err
}

func (vf *VFile) Read(buf []byte) (int, error) {
	if vf.f == vf {
		log.Fatal("wtf")
	}
	return vf.f.Read(buf)
}

// Close - close the file for writing. Also
// calls the volume recorder's AddFile()
func (vf *VFile) Close() error {
	err := vf.v.vm.AddFile(vf, vf.v)
	if err != nil {
		fmt.Println("got an error trying to addfile:", err.Error())
		return err
	}
	return vf.f.Close()
}

// Volume - represents a collection of file objects
type Volume struct {
	ID       string    `db:"id"`
	Created  time.Time `db:"created"`
	Name     string    `db:"name"`
	Salt     []byte    `db:"salt"`
	PWHash   []byte    `db:"pwhash"`
	Path     string    `db:"path"`
	TestMode bool      `db:"testmode"`
	Tags     []string
	Attrs    map[string]string
	vm       *VolumeManager
	files    map[string]*VFile
}

// GetPath - return the path up to the filename for the volume.
func (v *Volume) GetPath() string {
	return v.Path
}

// Open - return a VFile object, retrieved by the ID
func (v *Volume) Open(id string) (*VFile, error) {
	vf, err := v.vm.vr.GetFile(id, v)
	if err != nil {
		return nil, err
	}

	vf.Path = path.Join(v.GetPath(), vf.ID)

	if v.TestMode {
		ok := true
		vf, ok = v.files[id]
		if !ok {
			return nil, fmt.Errorf("file %q not found in memory", id)
		}
	} else {
		f, err := os.Open(vf.Path)
		if err != nil {
			return nil, err
		}
		vf.f = f
	}

	return vf, nil
}

// Create - create a new file object within the volume
func (v *Volume) Create(fb *filestore.FileBlob, tags []string, attrs map[string]string) (*VFile, error) {
	id := uuid.New().String()
	if fb.ID != "" {
		id = fb.ID
	}

	vf := VFile{
		ID:       id,
		v:        v,
		Filename: fb.Filename,
		Type:     fb.Type,
	}

	vf.Path = path.Join(v.GetPath(), vf.ID)

	if v.TestMode {
		vf.f = &FakeFile{}
		v.files[vf.ID] = &vf
	} else {
		fmt.Println("going to try to create:", vf.Path)
		f, err := os.Create(vf.Path)
		if err != nil {
			return nil, err
		}
		vf.f = f
	}
	return &vf, nil
}

// CheckPassword - compare a user provided password with
// the one stored with the volume.
func (v *Volume) CheckPassword(pw []byte) bool {
	return CheckPassword(pw, v.Salt, v.PWHash)
}

// CheckPassword - Check provided password against previously computed hash.
func CheckPassword(pw, salt, hash []byte) bool {
	pwhash, err := newPWHash(pw, salt)
	if err != nil {
		fmt.Println("problem generating password hash: ", err.Error())
		return false
	}
	res := bytes.Compare(pwhash, hash)
	return res == 0
}

// SignedToken - return a properly signed JWT token representing this OpenVolume session
func (v *Volume) SignedToken(skey []byte) (string, error) {
	ss, err := v.getJWT().SignedString(skey)
	return ss, err
}

func (v *Volume) getJWT() *jwt.Token {
	sm := jwt.SigningMethodHS256

	claims := VolumeClaims{
		//u.Roles,
		//"no token required",
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(24 * time.Hour).Unix(), // 24 hour token
			Issuer:    "filestore",
			Subject:   v.Name,
			Id:        v.ID,
		},
	}

	return jwt.NewWithClaims(sm, claims)
}

// DeleteFile - remove a file both from disk and
// from the database.
func (v *Volume) DeleteFile(id string) error {
	if v.TestMode {
		delete(v.files, id)
	} else {
		vf, err := v.Open(id)
		if err == nil {
			//vf.Close()
			os.Remove(vf.Path)
		}
	}
	return v.vm.DeleteFile(id, v.ID)
}

// VolumeClaims - this is where we store private volume specific data in the token.
type VolumeClaims struct {
	jwt.StandardClaims
}

// RandSeq - Create a random string of characters, length n. This is used
// as part of a password salt.
func randSeq(n int) []byte {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return []byte(string(b))
}

// NewPWHash - calculate a new password hash
func newPWHash(password []byte, salt []byte) ([]byte, error) {
	passwordhash, err := scrypt.Key(password, salt, 16384, 8, 1, 32)
	if err != nil {
		return nil, err
	}
	return passwordhash, nil
}

// GetTokenClaims - parse the claims structure from thetoken
func GetTokenClaims(tokenstr string, skey []byte) (*VolumeClaims, error) {
	var volumeClaims VolumeClaims

	token, err := jwt.ParseWithClaims(tokenstr, &volumeClaims, func(token *jwt.Token) (interface{}, error) {
		return skey, nil
	})

	if err != nil {
		fmt.Printf("*** problem parsing jwt: %s\n", err.Error())
		return nil, err
	}

	if claims, ok := token.Claims.(*VolumeClaims); ok {
		if token.Valid {
			return claims, nil
		}
		return nil, errors.New("invalid token")
	}
	return nil, errors.New("claims missing")
}

// VolumeManager - this structure is the controller for the set of active volumes
// In this case I'm doing it all in-memory, but for a production system this would
// actually talk to a database of some kind.
type VolumeManager struct {
	paths   map[string]string
	volumes map[string]*Volume
	vr      VolumeRecorder
}

// NewVolumeManager - allocate a new VolumeManager
func NewVolumeManager(vr VolumeRecorder, paths map[string]string) *VolumeManager {
	vm := VolumeManager{
		paths:   paths,
		volumes: make(map[string]*Volume),
		vr:      vr,
	}

	// Create the folders referenced in paths
	for _, p := range paths {
		os.Mkdir(p, 0700)
	}
	return &vm
}

// AddFile - Add a file record
func (vm *VolumeManager) AddFile(vf *VFile, v *Volume) error {
	return vm.vr.AddFile(vf, v)
}

// DeleteFile - Remove file from record
func (vm *VolumeManager) DeleteFile(id, volume_id string) error {
	return vm.vr.DeleteFile(id, volume_id)
}

// CreateVolume - Create a new volume.
func (vm *VolumeManager) CreateVolume(name, password string, tags []string, attrs map[string]string) (*Volume, error) {
	v, err := vm.vr.GetVolume(name, password, "")
	if err != nil {
		return nil, fmt.Errorf("couldn't check existing volumes: %w", err)
	}

	if v != nil {
		return nil, fmt.Errorf("volume already exists")
	}

	salt := randSeq(saltsize)
	hash, err := newPWHash([]byte(password), salt)
	if err != nil {
		return nil, fmt.Errorf("problem computing hash: %v", err)
	}

	// Now, check the tags. If "test" is present, then this is a test volume,
	// and all files created will be in-memory files rather than disk
	testmode := false
	for _, t := range tags {
		if t == "test" {
			testmode = true
		}
	}

	typ := "default"
	if val, ok := attrs["type"]; ok {
		typ = val
	}
	p, err := vm.GetPath(typ)
	if err != nil {
		return nil, fmt.Errorf("problem finding path type %q: %w",
			typ, err)
	}

	p = path.Join(p, name)
	fmt.Println("path is supposed to be:", p)
	err = os.Mkdir(p, 0755)
	if err != nil {
		return nil, fmt.Errorf("problem creating directory %q: %w", p, err)
	}

	v = &Volume{
		ID:       uuid.New().String(),
		Salt:     salt,
		PWHash:   hash,
		Name:     name,
		Tags:     tags,
		Attrs:    attrs,
		Path:     p,
		TestMode: testmode,
		vm:       vm,
		files:    make(map[string]*VFile),
	}

	err = vm.vr.AddVolume(v)
	if err != nil {
		return nil, fmt.Errorf("problem adding volume: %w", err)
	}
	return v, nil
}

// GetPath - Return the path indicated as specified in the config file.
// Example config is as so:
// [vmgr]
//  [vmgr.paths]
// 	  default = "/mnt/nasdrive/filestore"
//    fast = "/mnt/nasdrive/filestore"
func (vm *VolumeManager) GetPath(pathname string) (string, error) {
	if path, ok := vm.paths[pathname]; ok {
		return path, nil
	}
	return "", fmt.Errorf("not found")
}

// GetVolume - Find a volume based on a given name. This is needed by
// the engine so it can specify where the file goes
func (vm *VolumeManager) GetVolume(name, password, id string) (*Volume, error) {
	v, err := vm.vr.GetVolume(name, password, id)
	if err != nil {
		return nil, err
	}
	if v == nil {
		return nil, nil
	}
	v.vm = vm
	return v, nil
}
