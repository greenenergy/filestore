/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package app

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/greenenergy/filestore/cmd/service/filestore"
	pb "gitlab.com/greenenergy/filestore/cmd/service/filestore"
	grpc "google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// MyFilestoreServer - this is the actual implementation of the filestore server.
// It may make sense later to have different types of these, if we start working with
// vastly different file storage technologies.
type MyFilestoreServer struct {
	verbose bool
	volumes map[string]*Volume
	logs    *LogSet
	config  *viper.Viper
	vm      *VolumeManager
}

// NewMyFilestoreServer - allocate a new FilestoreServer object
func NewMyFilestoreServer(logs *LogSet, config *viper.Viper, vm *VolumeManager) *MyFilestoreServer {

	return &MyFilestoreServer{
		volumes: make(map[string]*Volume),
		config:  config,
		logs:    logs,
		vm:      vm,
	}
}

// CreateVolume - open a volume for reading or writing.
func (mfs *MyFilestoreServer) CreateVolume(ctx context.Context, vd *pb.VolumeDef) (*pb.VolumeDef, error) {
	skey := []byte(mfs.config.GetString("server.signingKey"))

	vol, err := mfs.vm.CreateVolume(vd.Name, vd.Password, vd.Tags, vd.Attrs)
	if err != nil {
		return nil, status.Error(codes.Internal,
			fmt.Sprintf("problem creating volume: %s", err.Error()))
	}

	st, err := vol.SignedToken(skey)
	if err != nil {
		return nil, err
	}
	vd.Token = st
	vd.Password = ""
	return vd, nil
}

// GetVolume - Return a pre-existing volume
func (mfs *MyFilestoreServer) GetVolume(ctx context.Context, vd *pb.VolumeDef) (*pb.VolumeDef, error) {
	skey := []byte(mfs.config.GetString("server.signingKey"))
	vol, err := mfs.vm.GetVolume(vd.Name, vd.Password, "")
	if err != nil {
		return nil, status.Error(codes.Internal, "problem getting volume")
	}

	if vol == nil {
		return nil, status.Error(codes.NotFound, "not found")
	}

	st, err := vol.SignedToken(skey)
	if err != nil {
		return nil, err
	}
	vd.Token = st
	vd.Password = ""
	vd.Attrs = vol.Attrs
	vd.Tags = vol.Tags
	return vd, nil
}

// Put - Transfer file to server
// This must be used whenever the file is more than 4mb, as that is the maximum default
// grpc message size.
func (mfs *MyFilestoreServer) Put(stream pb.FileStore_PutServer) error {
	var err error
	starttime := time.Now()
	written := 0
	removeFile := true

	defer func() {
		fmt.Printf("Put(%d bytes) finished in: %s\n", written, time.Now().Sub(starttime).Round(time.Millisecond))
	}()

	// NOTE: I think this should change. I like the idea of opening a volume to write into, and
	// you specify attributes that you want that volume to have, say "fast" or "longterm". Once
	// you've opened that volume, you now have a handle on the type of storage you wanted.
	// That means that the path won't be a single location in a config file. Instead,
	// there should probably be several paths in the config representing the roots of different
	// types of file storage. Then the VolumeManager would use that list of locations and types
	// when creating Volumes for incoming requests.

	var file *VFile
	reading := true

	// The gRPC streaming API sends the same message object type for each chunk,
	// so to reduce redundancy, only send the file details & metadata during the
	// first chunk.
	first := true
	iter := 0
	var volume *Volume
	for reading {
		iter++
		fb, err := stream.Recv()
		if mfs.verbose {
			fmt.Printf("r")
		}
		if err != nil {
			reading = false
			if err != io.EOF {
				mfs.logs.Error.Println("error reading:", err.Error())
				return err
			}
			break
		}

		if first {
			first = false
			var dummyfb pb.FileBlob
			dummyfb = *fb
			dummyfb.Payload = nil // We don't want to print the payload.

			skey := []byte(mfs.config.GetString("server.signingKey"))

			if fb.VolumeToken != "" {
				claims, err := GetTokenClaims(fb.VolumeToken, skey)
				if err != nil {
					mfs.logs.Error.Println("Problem getting token claims:", err)
					return err
				}

				volumeName := claims.Subject
				volumeID := claims.Id
				volume, err = mfs.vm.GetVolume(volumeName, "", volumeID)
				if err != nil {
					mfs.logs.Error.Println("Problem finding volume '", volumeName, "':", err)
					return err
				}

				if volume == nil {
					return status.Error(codes.NotFound, volumeName+" not found")
				}

				file, err = volume.Create(fb, nil, nil)

				if err != nil {
					mfs.logs.Error.Printf("problem creating file: %s", err)
					return err
				}

				defer func() {
					file.Close()
				}()

				defer func() {
					// If we leave this function before the end, we want the file we just opened
					// to be removed.
					if removeFile {
						os.Remove(file.Path)
					}
				}()
			} else {
				return status.Error(codes.PermissionDenied, "Token not provided")
			}
		}
		len, err := file.Write(fb.Payload)
		written += len
		if mfs.verbose {
			fmt.Printf("w")
			if iter%20 == 0 {
				fmt.Printf("\n")
			}
		}

		if err != nil {
			mfs.logs.Error.Println("error writing chunk:", err.Error())
			return err
		}
	}

	// If we make it to this point, then we want to keep the file as written on disk.
	removeFile = false
	nfb := pb.FileBlob{
		ID: file.ID,
	}

	err = stream.SendAndClose(&nfb)
	if err != nil {
		fmt.Printf("error returned from SendAndClose: %#v\n", err)
	}

	return err
}

const chunkSize = 64 * 1024

// Get - implement the GetFile command
func (mfs *MyFilestoreServer) Get(fb *filestore.FileBlob, stream filestore.FileStore_GetServer) error {
	skey := []byte(mfs.config.GetString("server.signingKey"))

	if fb.VolumeToken != "" {
		claims, err := GetTokenClaims(fb.VolumeToken, skey)
		if err != nil {
			mfs.logs.Error.Println("Problem getting token claims:", err)
			return err
		}

		volumeName := claims.Subject
		volumeID := claims.Id
		volume, err := mfs.vm.GetVolume(volumeName, "", volumeID)
		if err != nil {
			mfs.logs.Error.Println("Problem finding volume '", volumeName, "':", err)
			return err
		}
		if volume == nil {
			return status.Error(codes.NotFound, volumeName+" not found")
		}
		vf, err := volume.Open(fb.ID)
		if err != nil {
			mfs.logs.Error.Println("Problem getting file "+fb.ID+":", err)
			return err
		}
		buf := make([]byte, chunkSize)
		writing := true

		iter := 0
		for writing {
			n, err := vf.Read(buf)
			if err != nil {
				if err != io.EOF {
					mfs.logs.Error.Println("Problem getting file "+fb.ID+":", err)
					return err
				}
				writing = false
			}
			iter++
			fb := pb.FileBlob{
				Payload: buf[:n],
			}

			err = stream.Send(&fb)
			if err != nil {
				if err != io.EOF {
					mfs.logs.Error.Println("problem sending:", err.Error())
					return err
				}
				writing = false
			}
		}
	}
	return nil
}

// Delete - implement the DeleteFile command
func (mfs *MyFilestoreServer) Delete(ctx context.Context, fb *pb.FileBlob) (*pb.FileBlob, error) {
	if fb.VolumeToken == "" {
		return nil, status.Error(codes.Unauthenticated, "no token provided")
	}

	skey := []byte(mfs.config.GetString("server.signingKey"))

	claims, err := GetTokenClaims(fb.VolumeToken, skey)
	if err != nil {
		mfs.logs.Error.Println("Problem getting token claims:", err)
		return nil, err
	}

	volumeName := claims.Subject
	volumeID := claims.Id
	volume, err := mfs.vm.GetVolume(volumeName, "", volumeID)
	if err != nil {
		mfs.logs.Error.Println("Problem finding volume '", volumeName, "':", err)
		return nil, err
	}

	if volume == nil {
		return nil, status.Error(codes.NotFound, volumeName+" not found")
	}

	err = volume.DeleteFile(fb.ID)
	return nil, err
}

/*
// SetMeta - implement the SetFileMeta command
func (mfs *MyFilestoreServer) SetMeta(ctx context.Context, fb *pb.FileBlob) (*pb.FileState, error) {
	return nil, fmt.Errorf("unimplemented")
}

// GetMeta - implement the GetFileMeta command
func (mfs *MyFilestoreServer) GetMeta(ctx context.Context, fb *pb.FileBlob) (*pb.FileState, error) {
	return nil, fmt.Errorf("unimplemented")
}

// AddTag - implement the AddTag command
func (mfs *MyFilestoreServer) AddTag(ctx context.Context, ft *pb.FileTag) (*pb.FileState, error) {
	return nil, fmt.Errorf("unimplemented")
}

// DelTag - implement the DelTag command
func (mfs *MyFilestoreServer) DelTag(ctx context.Context, ft *pb.FileTag) (*pb.FileState, error) {
	return nil, fmt.Errorf("unimplemented")
}
*/

// RunServer - this is the entrypoint of the GRPC file storage service
func (mfs *MyFilestoreServer) RunServer() error {
	starttime := time.Now()
	srv := grpc.NewServer()
	filestore.RegisterFileStoreServer(srv, mfs)
	listenAddr := mfs.config.GetString("server.grpcAddr")
	mfs.logs.Debug.Println("listenAddr:", listenAddr)
	if listenAddr == "" {
		mfs.logs.Error.Printf("problem listening - no address provided")
	}
	lis, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Fatalf("could not listen to %q: %v", listenAddr, err)
	}
	errChan := make(chan error, 10)
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	go func(ec chan error) {
		err := srv.Serve(lis)
		if err != nil {
			ec <- err
			return
		}
	}(errChan)

	for {
		select {
		case err := <-errChan:
			return err

		case s := <-signalChan:
			mfs.logs.Debug.Printf("Captured %v, exiting", s)
			mfs.logs.Debug.Println("Ran for", time.Now().Sub(starttime).Round(time.Millisecond).String())
			os.Exit(0)
		}
	}
}
