/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package app

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // This  is the postgres driver for sqlx
)

type pgargs struct {
	Host string `json:"host"`
	Port int    `json:"port"`
	Name string `json:"name"`
	Pass string `json:"pass"`
	User string `json:"user"`
}

// PostgresVolumeRecorder - This is the volume recorder which uses Postgres as the persistant store
type PostgresVolumeRecorder struct {
	pgargs
	conn *sqlx.DB
}

// NewPostgresVolumeRecorder - Allocate a new PG recorder
func NewPostgresVolumeRecorder(cfg map[string]interface{}) (VolumeRecorder, error) {
	var pvr PostgresVolumeRecorder

	// The Marshal/Unmarshal approach is not as efficient as it could be,
	// but this only happens once at startup, so effiency doesn't matter
	// as much as code clarity & simplicity.
	t, err := json.Marshal(cfg)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(t, &pvr)
	if err != nil {
		return nil, err
	}
	// Now actually try to connect to postgres
	mode := "disable"
	if false {
		mode = "ssl-verify"
	}

	connStr := fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=%s password=%s",
		pvr.Host, pvr.Port, pvr.User, pvr.Name, mode, pvr.Pass)

	fmt.Println("attempting to connect to postgres using: ", connStr)
	conn, err := sqlx.Open("postgres", connStr)

	if err != nil {
		return nil, fmt.Errorf("problem connecting to postgres (%s) : %w", connStr, err)
	}
	pvr.conn = conn

	// Here is the connection loop. If there is a database connection
	// problem, this is where we will detect it, and handle the retries.
	success := false
	retries := 10 // how many times to try connecting to db
	for x := 0; x < retries && !success; x++ {
		_, err = conn.Queryx("select count(*) from volumes")
		if err != nil {
			if _, ok := err.(*net.OpError); ok {
				fmt.Println("this is a network error, gonna retry:", err.Error())
				time.Sleep(time.Second)
			} else {
				/*
					if _, ok := err.(pq.Error) {

						// pq: database "filestore" does not count
					}
				*/
				fmt.Printf("Got an error trying to get the volume count: %T\n", err)
				log.Fatal(err.Error())
			}
		} else {
			success = true
		}
	}

	if !success {
		fmt.Println("couldn't find the db")
		log.Fatal("no database")
	}

	return &pvr, nil
}

// AddVolume - Add volume to records
func (pvr *PostgresVolumeRecorder) AddVolume(v *Volume) error {
	_, err := pvr.conn.Exec("insert into volumes(id, name, salt, pwhash, path, testmode) values ($1, $2, $3, $4, $5, $6)",
		v.ID, v.Name, v.Salt, v.PWHash, v.Path, v.TestMode)
	if err != nil {
		return fmt.Errorf("problem inserting volume: %w", err)
	}

	for _, tag := range v.Tags {
		if tag != "" {
			_, err = pvr.conn.Exec("insert into volume_tags(volume_id, tag) values ($1, $2)", v.ID, tag)
			if err != nil {
				return fmt.Errorf("problem adding tag %q: %w", tag, err)
			}
		}
	}

	for key, val := range v.Attrs {
		_, err = pvr.conn.Exec("insert into volume_attrs(volume_id, key, val) values ($1, $2, $3)", v.ID, key, val)
		if err != nil {
			return fmt.Errorf("problem adding attr (%q:%q): %w", key, val, err)
		}
	}

	return nil
}

// GetVolume - return a volume from the records.
// The third argument "id" is there as an override. If this volume
// is being used as part of a 'put' or 'get' operation, then
// the password won't be provided, as it's not part of the token.
// The ID of the volume is however, and you can only get the volume
// ID if you open the volume with the password, so we have that
// level of security.
func (pvr *PostgresVolumeRecorder) GetVolume(name, password, id string) (*Volume, error) {
	var rows *sqlx.Rows
	var err error
	common := "select id, created, name, salt, pwhash, path from volumes where"

	if id != "" {
		rows, err = pvr.conn.Queryx(common+" id = $1", id)
	} else {
		rows, err = pvr.conn.Queryx(common+" name = $1", name)
	}

	if err != nil {
		return nil, fmt.Errorf("problem querying volumes: %w", err)
	}
	defer rows.Close()

	for rows.Next() {
		var v Volume
		err = rows.StructScan(&v)
		if err != nil {
			return nil, fmt.Errorf("problem with structscan: %w", err)
		}
		if v.Name == name {
			if id != "" || v.CheckPassword([]byte(password)) {
				// Since the password matches (or the overrid ID was provided), pull the attrs & tags too
				tags, err := pvr.conn.Queryx("select tag from volume_tags where volume_id = $1", v.ID)
				if err != nil {
					return nil, fmt.Errorf("problem pulling tags: %w", err)
				}
				defer tags.Close()

				for tags.Next() {
					var tag string
					err = tags.Scan(&tag)
					if err != nil {
						return nil, fmt.Errorf("problem scanning tag: %w", err)
					}
					v.Tags = append(v.Tags, tag)
				}
				v.Attrs = make(map[string]string)

				attrRows, err := pvr.conn.Queryx("select key, val from volume_attrs where volume_id = $1", v.ID)
				if err != nil {
					return nil, fmt.Errorf("problem pulling tags: %w", err)
				}
				defer attrRows.Close()

				for attrRows.Next() {
					var key, val string
					err = attrRows.Scan(&key, &val)
					if err != nil {
						return nil, fmt.Errorf("problem scanning tag: %w", err)
					}
					v.Attrs[key] = val
				}

				return &v, nil
			}
			return nil, fmt.Errorf("password mismatch")
		}
	}
	return nil, nil
}

// AddFile - Add a file record
func (pvr *PostgresVolumeRecorder) AddFile(vf *VFile, v *Volume) error {
	q := `insert into files(id, volume_id, filename, size, type)
	 values ($1, $2, $3, $4, $5)`
	_, err := pvr.conn.Exec(q, vf.ID, v.ID, vf.Filename, vf.Size, vf.Type)
	return err
}

// DeleteFile - Remove file from records
func (pvr *PostgresVolumeRecorder) DeleteFile(id, volume_id string) error {
	q := `delete from files where id = $1 and volume_id = $2`
	_, err := pvr.conn.Exec(q, id, volume_id)
	return err
}

// GetFile - Return file data, specifically the path
func (pvr *PostgresVolumeRecorder) GetFile(id string, vol *Volume) (*VFile, error) {
	q := `select id, filename, size, type from files where volume_id = $1 and id = $2`
	var vf VFile
	err := pvr.conn.QueryRow(q, vol.ID, id).Scan(&vf.ID, &vf.Filename, &vf.Size, &vf.Type)
	if err != nil {
		return nil, fmt.Errorf("problem reading file from db: %w", err)
	}
	return &vf, nil
}
