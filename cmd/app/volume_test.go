package app

import (
	"encoding/json"
	"strings"
	"testing"
)

const (
	skey         = "test_signing_key"
	testPassword = "password1"
	testVolname  = "testvolume"
)

// startUp - create a volume manager
func startUp(t *testing.T) (*VolumeManager, *Volume) {
	configPaths := []byte(`{
		"default":"/tmp/paths/default",
		"fast":"/tmp/paths/fast"
	}`)

	paths := make(map[string]string)
	err := json.Unmarshal(configPaths, &paths)
	if err != nil {
		t.Fatal(err)
	}

	vm := NewVolumeManager(NewMemVolumeRecorder(), paths)

	// By specifying "test" as one of the volume tags, we know that
	// all files created will be "fake" in-memory files.
	voltags := []string{"fast", "test"}
	volattrs := map[string]string{
		"mickey": "mouse",
	}

	volume, err := vm.CreateVolume(testVolname, testPassword, voltags, volattrs)
	if err != nil {
		t.Fatal(err)
	}

	return vm, volume
}

func TestBasicVolumeFuncs(t *testing.T) {
	_, volume := startUp(t)

	tags := []string{"a", "b", "c"}
	attrs := map[string]string{
		"filename": "dummyfile",
	}

	vf, err := volume.Create("dummyfile", tags, attrs)
	if err != nil {
		t.Fatal(err)
	}

	dummydata := []byte("this is dummy data for testing")

	numbytes, err := vf.Write(dummydata)
	if err != nil {
		t.Fatal(err)
	}

	if numbytes != len(dummydata) {
		t.Fatalf("should have written %d, instead wrote %d",
			len(dummydata), numbytes)
	}

	err = vf.Close()
	if err != nil {
		t.Fatal(err)
	}

	v2, err := volume.Open(vf.ID)
	if err != nil {
		t.Fatal(err)
	}

	output := make([]byte, 500)
	len, err := v2.Read(output)
	if err != nil {
		t.Fatal(err)
	}

	should := string(dummydata)
	was := string(output[:len])
	res := strings.Compare(should, was)
	if res != 0 {
		t.Fatalf("File read should have returned %q, instead returned %q",
			should, was)
	}

	err = volume.DeleteFile(vf.ID)
	if err != nil {
		t.Fatal(err)
	}
}

func xTestVolumePass(t *testing.T) {
	vm, volume := startUp(t)

	v2, err := vm.GetVolume(testVolname, "thisisnotthe password", "")
	if err == nil {
		t.Fatal("should have returned a bad password error")
	}

	v2, err = vm.GetVolume(testVolname, testPassword, "")
	if err != nil {
		t.Fatal(err)
	}

	if v2 == nil {
		t.Fatal("should have found the volume")
	}

	if v2.Name != volume.Name {
		t.Fatalf("found wrong volume. Should be %q, was %q",
			volume.Name, v2.Name)
	}

	f1, err := volume.vm.vr.GetFile("thisdoesntexist", volume)
	if err == nil {
		t.Fatal(err)
	}
	if f1 != nil {
		t.Fatal("f1 should be empty")
	}
}

func xTestFiles(t *testing.T) {
	_, volume := startUp(t)

	_, err := volume.Open("")
	if err == nil {
		t.Fatalf("should have failed finding file with empty name")
	}

	tags := []string{"a", "b", "c"}
	attrs := map[string]string{
		"filename": "dummyfile",
	}

	vf, err := volume.Create("dummyfile", tags, attrs)
	if err != nil {
		t.Fatal(err)
	}

	dummydata := []byte("this is dummy data for testing")

	was, err := vf.Write(dummydata)
	if err != nil {
		t.Fatal(err)
	}

	should := len(dummydata)
	if should != was {
		t.Fatalf("should have written %d bytes, only wrote %d", should, was)
	}
}
