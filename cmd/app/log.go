/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package app

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

// Out - Output wrapper
type Out struct {
	s fmt.Stringer
	w io.Writer
}

func (o Out) String() string {
	return o.s.String()
}

func (o *Out) Write(input []byte) (int, error) {
	var b bytes.Buffer

	b.Write([]byte(o.String()))
	b.Write(input)

	return o.w.Write(b.Bytes())
}

// WrapOut - instantiate an output wrapper
func WrapOut(w io.Writer, s fmt.Stringer) io.Writer {
	return &Out{s: s, w: w}
}

// TimeStruct - so we can override the time format
type TimeStruct struct{}

var (
	customTimeFormat = "2006-01-02T15:04:05.000Z07:00"
)

func (ts TimeStruct) String() string {
	return time.Now().Format(customTimeFormat)
}

// LogSet - the control structure for the log server.
type LogSet struct {
	Info     *log.Logger
	Error    *log.Logger // Error still just sends to Stdout
	TeeError *log.Logger // TeeError sends to both Stdout and Stderr
	Debug    *log.Logger
	Warn     *log.Logger
}

// NewLogSet - Allocate a new LogSet
func NewLogSet() *LogSet {
	return &LogSet{}
}

// Defaults - establish the standard defaults for this logset, which means
// establishing both stdout and stderr connections
func (ls *LogSet) Defaults(out io.Writer) {
	if out == nil {
		out = os.Stdout
	}
	stdPrefix := WrapOut(out, TimeStruct{})
	errPrefix := WrapOut(io.MultiWriter(out, os.Stderr), TimeStruct{})

	ls.Warn = log.New(stdPrefix, " [ WARN] ", log.Lshortfile)
	ls.Debug = log.New(stdPrefix, " [DEBUG] ", log.Lshortfile)
	ls.Info = log.New(stdPrefix, " [ INFO] ", 0)
	ls.Error = log.New(stdPrefix, " [ERROR] ", log.Llongfile)
	ls.TeeError = log.New(errPrefix, " [ERROR] ", log.Llongfile)
}
