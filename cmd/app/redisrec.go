package app

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"encoding/json"
	"fmt"

	"github.com/go-redis/redis/v7"
)

type redisVolumeRecorder struct {
	client *redis.Client
}

// NewRedisVolumeRecorder - Allocate a new redis backend handler
func NewRedisVolumeRecorder(host string, db int) (VolumeRecorder, error) {
	fmt.Printf("Attempting to connect to redis at: %q,0\n", host)
	client := redis.NewClient(&redis.Options{
		Addr:     host,
		Password: "",
		DB:       db,
	})

	return &redisVolumeRecorder{
		client: client,
	}, nil
}

func (rvr *redisVolumeRecorder) AddVolume(v *Volume) error {
	js, err := json.Marshal(v)
	if err != nil {
		return err
	}

	vid := fmt.Sprintf("volume:%s:%s", v.Name, v.ID)
	_, err = rvr.client.Set(vid, js, 0).Result()
	return err
}

// Internal-only function that can bypass the password checking
// The keys are in the form name:id, so in this case we will wildcard
// whichever part is not defined.
func (rvr *redisVolumeRecorder) getVolumeBy(id, name string) (*Volume, error) {
	first := "*"
	if name != "" {
		first = name
	}

	second := "*"
	if id != "" {
		second = id
	}

	keyref := fmt.Sprintf("volume:%s:%s", first, second)
	if keyref == "volume:*:*" {
		return nil, fmt.Errorf("must provide either name or ID")
	}

	fmt.Println("going to try to fetch this volume:", keyref)
	results, err := rvr.client.Keys(keyref).Result()
	if err != nil {
		return nil, err
	}

	if len(results) > 1 {
		return nil, fmt.Errorf("Should only have one match for name or ID")
	}
	if len(results) == 0 {
		// It's perfectly legal to request a volume that doesn't exist, as
		// this is part of the creation process
		return nil, nil
	}

	var vol Volume

	val, err := rvr.client.Get(results[0]).Result()
	err = json.Unmarshal([]byte(val), &vol)
	if err != nil {
		return nil, err
	}

	return &vol, nil
}

func (rvr *redisVolumeRecorder) GetVolume(name, password, id string) (*Volume, error) {
	v, err := rvr.getVolumeBy(id, name)
	if err != nil {
		return nil, err
	}
	if v == nil {
		return nil, nil
	}

	if id != "" || v.CheckPassword([]byte(password)) {
		return v, nil
	}
	return nil, fmt.Errorf("password mismatch")
}

func (rvr *redisVolumeRecorder) AddFile(vf *VFile, v *Volume) error {
	fkey := fmt.Sprintf("file:%s:%s", vf.ID, v.ID)
	data, err := json.Marshal(vf)
	if err != nil {
		return err
	}
	_, err = rvr.client.Set(fkey, data, 0).Result()
	return err
}

func (rvr *redisVolumeRecorder) DeleteFile(id, volID string) error {
	fkey := fmt.Sprintf("file:%s:%s", id, volID)
	_, err := rvr.client.Del(fkey).Result()
	return err
}

func (rvr *redisVolumeRecorder) GetFile(id string, v *Volume) (*VFile, error) {
	fkey := fmt.Sprintf("file:%s:%s", id, v.ID)
	val, err := rvr.client.Get(fkey).Result()
	if err != nil {
		return nil, err
	}

	var vf VFile
	err = json.Unmarshal([]byte(val), &vf)
	if err != nil {
		return nil, err
	}
	return &vf, nil
}
