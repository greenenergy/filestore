/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package app

import (
	"fmt"
)

// MemVolumeRecorder - this structure is the controller for the set of active volumes
// In this case I'm doing it all in-memory, but for a production system this would
// actually talk to a database of some kind.
type MemVolumeRecorder struct {
	volumes map[string]*Volume
	files   map[string]*VFile
}

// NewMemVolumeRecorder - allocate a new VolumeRecorder
func NewMemVolumeRecorder() VolumeRecorder {
	return &MemVolumeRecorder{
		volumes: make(map[string]*Volume),
		files:   make(map[string]*VFile), // file paths
	}
}

// AddVolume -
func (vr *MemVolumeRecorder) AddVolume(v *Volume) error {
	if _, ok := vr.volumes[v.Name]; ok {
		return fmt.Errorf("volume %q already exists", v.Name)
	}

	vr.volumes[v.Name] = v
	return nil
}

// GetVolume - Find a volume based on a given name. This is needed by
// the engine so it can specify where the file goes. If the ID is provided,
// then the password check is overridden.
func (vr *MemVolumeRecorder) GetVolume(name, password, id string) (*Volume, error) {
	if v, ok := vr.volumes[name]; ok {
		if id != "" || v.CheckPassword([]byte(password)) {
			return v, nil
		}
		return nil, fmt.Errorf("bad password")
	}
	// "not found" isn't actually an error
	return nil, nil
}

// AddFile - add the file to the record of files.
func (vr *MemVolumeRecorder) AddFile(vf *VFile, volume *Volume) error {
	if _, ok := vr.files[vf.ID]; ok {
		return fmt.Errorf("file %q already exists", vf.ID)
	}
	vr.files[vf.ID] = vf
	return nil
}

// DeleteFile - Remove any record of the file.
func (vr *MemVolumeRecorder) DeleteFile(id, volumeID string) error {
	if _, ok := vr.files[id]; !ok {
		return fmt.Errorf("file %q not found", id)
	}
	delete(vr.files, id)
	return nil
}

// GetFile - return a file path.
func (vr *MemVolumeRecorder) GetFile(id string, vol *Volume) (*VFile, error) {

	if vf, ok := vr.files[id]; ok {
		return vf, nil
	}

	return nil, fmt.Errorf("file %q not found", id)
}
