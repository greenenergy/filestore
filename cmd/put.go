/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"time"

	"github.com/h2non/filetype"
	"github.com/spf13/cobra"
	pb "gitlab.com/greenenergy/filestore/cmd/service/filestore"
)

// For Put & Get, the files are broken up into chunkSize chunks.
const chunkSize = 64 * 1024

// putCmd represents the put command
var putCmd = &cobra.Command{
	Use:   "put <filename>",
	Short: "store a file into the filestore",
	Long:  `Store  a file into the filestore`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Println("missing filename")
			return
		}
		filename := args[0]

		verbose := cmd.Flags().Lookup("verbose").Value.String() == "true"
		token := cmd.Flags().Lookup("token").Value.String()
		id := cmd.Flags().Lookup("id").Value.String()

		tmpbuf, _ := ioutil.ReadFile(filename)
		kind, _ := filetype.Match(tmpbuf)

		var mimeType string

		if kind == filetype.Unknown {
			mimeType = "application/octet-stream"
		} else {
			mimeType = kind.MIME.Value
		}
		fmt.Printf("file type: %s. Mime: %s\n", kind.Extension, kind.MIME.Value)

		file, err := os.Open(filename)
		if err != nil {
			logs.Error.Printf("problem opening %q: %v", filename, err)
			return
		}

		client, err := GetClient()
		if err != nil {
			logs.Error.Printf("problem connecting to client: %v", err)
			return
		}

		ctx := context.Background()
		stream, err := client.Put(ctx)
		if err != nil {
			logs.Error.Printf("problem getting stream handle: %v", err)
			return
		}

		buf := make([]byte, chunkSize)
		writing := true
		first := true

		accum := 0
		iter := 0
		for writing {
			n, err := file.Read(buf)
			iter++
			if verbose {
				fmt.Printf("r")
			}

			if err != nil {
				if err != io.EOF {
					logs.Error.Println("problem reading file blob:", err.Error())
				}
				writing = false
				break
			}
			accum += n
			fb := pb.FileBlob{
				Payload: buf[:n],
			}
			// In a grpc stream, the same object type is sent with every
			// chunk. There is no point in sending the duplicated metadata
			// with every chunk, so we only send it with the first chunk.
			if first {
				fb.ID = id
				fb.VolumeToken = token
				fb.Filename = filename
				tags, attrs := GetTagsAttrs(cmd.Flags())
				fb.Tags = tags
				fb.Attrs = attrs
				fb.Type = mimeType
			}
			err = stream.Send(&fb)
			if verbose {
				fmt.Printf("w")
				if iter%10 == 0 {
					fmt.Printf("\n")
				}
			}

			if err != nil {
				writing = false
				logs.Error.Printf("problem sending file chunk: %v", err)
				break
			}
		}

		var fb *pb.FileBlob
		fb, err = stream.CloseAndRecv()
		if err != nil {
			logs.Error.Println("error:", err.Error())
			return
		}
		tmps, _ := json.MarshalIndent(fb, "", "    ")
		logs.Debug.Println("file state after put:", string(tmps), "took", time.Now().Sub(starttime).Round(time.Millisecond))
	},
}

func init() {
	rootCmd.AddCommand(putCmd)

	putCmd.Flags().StringP("id", "i", "", "Set the ID of the file, rather than allowing the auto generated one")
	putCmd.Flags().StringP("token", "T", "", "Token for the volume you wish to write to")
	putCmd.Flags().StringP("tags", "t", "", "Tags to set. Simple comma separated list, no spaces.")
	putCmd.Flags().StringSliceP("attributes", "a", []string{}, "attributes to set. Use one '-a x=y' per attribute")
}
