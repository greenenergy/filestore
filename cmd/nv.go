/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package cmd

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	pb "gitlab.com/greenenergy/filestore/cmd/service/filestore"
)

// nvCmd represents the nv command
var nvCmd = &cobra.Command{
	Use:   "nv <name>",
	Short: "create a new volume",
	Long: `create a new volume. You can provide tags and attributes
when creating a new volume. One of the tags is "test". If you
set this, then a memory volume recorder is used, and files are only written
to memory. This is useful for test cases.`,
	Run: func(cmd *cobra.Command, args []string) {

		if len(args) < 1 {
			fmt.Println("missing filename")
			return
		}
		volname := args[0]
		tags, attrs := GetTagsAttrs(cmd.Flags())

		client, err := GetClient()
		if err != nil {
			logs.Error.Printf("problem connecting to client: %v", err)
			return
		}

		ctx := context.Background()
		pw := cmd.Flags().Lookup("password").Value.String()

		vd := pb.VolumeDef{
			Name:     volname,
			Attrs:    attrs,
			Tags:     tags,
			Password: pw,
		}

		res, err := client.CreateVolume(ctx, &vd)
		if err != nil {
			logs.Error.Printf("problem getting stream handle: %v", err)
			return
		}
		tmps, _ := json.MarshalIndent(res, "", "    ")
		fmt.Println("res: ", string(tmps))

	},
}

func init() {
	rootCmd.AddCommand(nvCmd)
	nvCmd.Flags().StringP("tags", "t", "", "Tags to set. Simple comma separated list, no spaces.")
	nvCmd.Flags().StringSliceP("attributes", "a", []string{}, "attributes to set. Use one '-a x=y' per attribute")
	nvCmd.Flags().StringP("password", "p", "", "Specify the password for the volume")
}
