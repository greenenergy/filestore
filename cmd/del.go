/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	pb "gitlab.com/greenenergy/filestore/cmd/service/filestore"
)

// delCmd represents the del command
var delCmd = &cobra.Command{
	Use:   "del",
	Short: "delete one or more files",
	//Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Println("usage: filestore del <fileid>")
			return
		}

		token := cmd.Flags().Lookup("token").Value.String()

		fb := pb.FileBlob{
			ID:          args[0],
			VolumeToken: token,
		}

		client, err := GetClient()
		if err != nil {
			logs.Error.Printf("problem connecting to client: %v\n", err)
			return
		}

		ctx := context.Background()
		_, err = client.Delete(ctx, &fb)
		if err != nil {
			logs.Error.Printf("problem deleting file object: %v\n", err)
			return
		}
	},
}

func init() {
	rootCmd.AddCommand(delCmd)
	delCmd.Flags().StringP("token", "T", "", "Token for the volume you wish to write to")
}
