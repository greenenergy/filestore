/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// setCmd represents the set command
var setCmd = &cobra.Command{
	Use:   "set",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
}

// setAttrCmd sets an attribute
var setAttrCmd = &cobra.Command{
	Use:   "attr",
	Short: "set one or more attributes",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("set attr called")
	},
}

// setTagCmd sets an attribute
var setTagCmd = &cobra.Command{
	Use:   "tag",
	Short: "set one or more tags",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("set tag called")
	},
}

func init() {
	rootCmd.AddCommand(setCmd)

	setCmd.AddCommand(setAttrCmd)
	setCmd.AddCommand(setTagCmd)
}
