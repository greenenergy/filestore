createuser fs -P <useapassword>
createdb filestore -O fs

psql -U fs -f /tmp/schema.sql filestore
