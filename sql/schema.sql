set timezone='America/Vancouver'

create table volumes (
	id text primary key,
	created timestamp with time zone not null default CURRENT_TIMESTAMP,
	name text not null unique,
	salt bytea,
	pwhash bytea,
	path text not null,
	testmode bool not null default 'false'
);

create table volume_attrs (
	id serial primary key,
	volume_id text not null references volumes(id) on delete cascade,
	key text,
	val text,
	UNIQUE(volume_id, key)
);

create table volume_tags (
	id serial primary key,
	volume_id text not null references volumes(id) on delete cascade,
	tag text not null,
	UNIQUE(volume_id, tag)
);

create table files (
	id text primary key,
	volume_id text not null references volumes(id),
	created timestamp with time zone not null default CURRENT_TIMESTAMP,
	filename text not null,	-- No need to store the whole path, as that comes from the volume. Saves space to just record the filename.
	type text not null default '',
	size int not null default 0
);

-- Not using an 'on delete cascade' with files, as each file also
-- needs to be deleted from physical storage. If we nuke the file entry
-- here we will have no record of those files to know which to remove from
-- storage.
