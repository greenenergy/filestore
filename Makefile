GITHASH := `git rev-parse HEAD`
FILEGITHASH := `cat .githash`
PROJNAME := filestore
VERSION := 0.1


FILES:=cmd/del.go cmd/get.go cmd/list.go cmd/put.go cmd/root.go \
	cmd/server.go cmd/service/filestore/fs.proto cmd/set.go cmd/service/filestore/fs.pb.go \
	cmd/open.go cmd/version.go cmd/version.go cmd/nv.go cmd/misc.go \
	cmd/app/volume.go cmd/app/engine.go cmd/app/log.go cmd/app/memrec.go cmd/app/pgrec.go \
	main.go .githash cmd/app/redisrec.go

filestore: $(FILES)
	CGO_ENABLED=0 GOOS=linux go build -ldflags "-s -w -extldflags '-static' -X main.githash=$(FILEGITHASH)" -o $(PROJNAME) -a -installsuffix cgo

.githash:
	echo $(GITHASH) >.githash

cmd/service/filestore/fs.pb.go: cmd/service/filestore/fs.proto
	@protoc -I cmd/service/filestore cmd/service/filestore/fs.proto --go_out=plugins=grpc:cmd/service/filestore

# If you are building for minikube, you need to use the docker daemon that comes with it. Here is how you do that.
minidocker: .githash $(PROJNAME)
	(eval $$(minikube docker-env); docker build -t greenenergy/filestore:latest .)

docker: .githash $(PROJNAME)
	docker build -t greenenergy/filestore:latest .


testcover:
	go test -coverprofile=coverage.out ./...
	go tool cover -func=coverage.out
	go tool cover -html=coverage.out

docker-export-file:
	docker save filestore -o filestore.tar

docker-export: docker
	docker tag greenenergy/filestore gcr.io/kazooky-portal/filestore:$(VERSION)
	docker tag greenenergy/filestore gcr.io/kazooky-portal/filestore:latest
	docker push gcr.io/kazooky-portal/filestore:$(VERSION)
	docker push gcr.io/kazooky-portal/filestore:latest
